//
//  MobileConfigDomain.swift
//  masher
//
//  Created by Joel Rennich on 6/12/20.
//  Copyright © 2020 Orchard & Grove, Inc. All rights reserved.
//

import Foundation

let kManagedClientDomain = "com.apple.ManagedClient.preferences"

indirect enum MobileConfigValue: Codable {
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let value = try? container.decode(Bool.self) {
            self = .bool(value)
        } else if let value = try? container.decode(Int.self) {
            self = .int(value)
        } else if let value = try? container.decode(String.self) {
            self = .string(value)
        } else if let value = try? container.decode([MobileConfigValue].self) {
            self = .array(value)
        } else if let value = try? container.decode([String:MobileConfigValue].self) {
            self = .object(value)
        } else {
            throw DecodingError.typeMismatch(MobileConfigValue.self, DecodingError.Context(codingPath: container.codingPath, debugDescription: "Not a String or [String]"))
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .bool(let value):
            try container.encode(value)
        case .int(let value):
            try container.encode(value)
        case .string(let value):
            try container.encode(value)
        case .data(let value):
            try container.encode(value)
        case .array(let value):
            try container.encode(value)
        case .object(let value):
            try container.encode(value)
        }
    }
    
    case string(String)
    case bool(Bool)
    case int(Int)
    case data(Data)
    case object([String:MobileConfigValue])
    case array([MobileConfigValue])
    
    func stringValue() -> String? {
        switch self {
        case .string(let value):
            return value
        default:
            return nil
        }
    }
    
    func boolValue() -> Bool? {
        switch self {
        case .bool(let value):
            return value
        default:
            return nil
        }
    }
}

struct MobileConfigDomain: Decodable {
    var domain: [String:MobileConfigValue]
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        do {
            domain = try container.decode([String:MobileConfigValue].self)
        } catch {
            throw DecodingError.valueNotFound(MobileConfigValue.self, DecodingError.Context(codingPath: container.codingPath, debugDescription: "Not a MobileConfigValue"))
        }
    }
    
    func validate() -> Bool? {
        
        var result: Bool?
        
        for domain in self.domain {
            
            var tempDomain = domain
            
            if domain.key == kManagedClientDomain {
                switch domain.value {
                case .object(let value):
                    if let firstValue = value.first {
                        tempDomain.key = firstValue.key
                        tempDomain.value = firstValue.value
                    }
                default:
                    break
                }
            }
            
            switch tempDomain.value {
            case .object(let config):
                if let defaults = UserDefaults.init(suiteName: tempDomain.key) {
                    for object in config {
                        if defaults.objectIsForced(forKey: object.key),
                            let objectValue = defaults.object(forKey: object.key) {
                            
                            switch object.value {
                            case .bool(let value):
                                if let finalValue = objectValue as? Bool,
                                    finalValue == value {
                                    result = true
                                } else {
                                    return false
                                }
                            case .int(let value):
                                if let finalValue = objectValue as? Int,
                                    finalValue == value {
                                    result = true
                                } else {
                                    return false
                                }
                            case .string(let value):
                                if let finalValue = objectValue as? String,
                                    finalValue == value {
                                    result = true
                                } else {
                                    return false
                                }
                            default:
                                break
                            }
                        } else {
                            return false
                        }
                    }
                } else {
                    return false
                }
            case .array(let values):
                print("ARRAY!!!")
            default:
                break
            }
        }
        return result
    }
}
