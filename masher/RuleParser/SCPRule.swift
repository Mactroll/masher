//
//  Rule.swift
//  masher
//
//  Created by Joel Rennich on 6/11/20.
//  Copyright © 2020 Orchard & Grove, Inc. All rights reserved.
//

import Foundation

struct SCPRule: Decodable {
    let id: String
    let title: String
    let result: [String:String]?
    let discussion: String
    let check: String
    let fix: String
    let references: [String:[String]]
    let macOS: [String]
    let tags: [String]
    let mobileconfig: Bool?
    let mobileconfigInfo: MobileConfigDomain?
    var finding: Bool?
    
    enum CodingKeys: String, CodingKey {
        case id, title, result, discussion, check, fix, references, macOS, tags, mobileconfig
        case mobileconfigInfo = "mobileconfig_info"
    }
    
    mutating func checkRule() -> Bool {
        
        if let configInfo = mobileconfigInfo {
            self.finding = configInfo.validate()
            if self.finding != true {
                createMobileConfig()
            }
        } else {
            self.finding = runTask(arugments: self.check)
        }
        
        return self.finding ?? false
    }
    
    fileprivate func runTask(arugments: String) -> Bool? {
        
        if arugments.first != "/" {
            return nil
        }
        
        let task = Process()
        task.launchPath = "/bin/bash"
        task.arguments = ["-c", arugments.replacingOccurrences(of: "$CURRENT_USER", with: NSUserName())]
        let outpipe = Pipe()
        let errorPipe = Pipe()
        task.standardOutput = outpipe
        task.standardError = errorPipe
        task.launch()
        task.waitUntilExit()
        
        let data = outpipe.fileHandleForReading.readDataToEndOfFile()
        if let resultString = String(data: data, encoding: .utf8)?.trimmingCharacters(in: .newlines),
            let resultExpected = self.result?.first?.value {
            return resultString == resultExpected
        }
        return nil
    }
    
    fileprivate func createMobileConfig() {
        let encoder = PropertyListEncoder.init()
        encoder.outputFormat = .xml
        guard let domain = self.mobileconfigInfo?.domain else { return }
        let profileItems = MobileConfigurationProfile.init(rule: self, payload: domain)
        if let profile = try? encoder.encode(profileItems),
            //let path = URL.init(string: "/tmp/profile-\(self.id).mobileconfig"),
            let profileString = String.init(data: profile, encoding: .utf8) {
            try? profileString.write(toFile: "/tmp/profile-\(self.id).mobileconfig", atomically: true, encoding: .utf8)
        }
    }
}

