//
//  SCPRules.swift
//  masher
//
//  Created by Joel Rennich on 6/11/20.
//  Copyright © 2020 Orchard & Grove, Inc. All rights reserved.
//

import Foundation

struct SCPRules {
    
    var rules = [String:SCPRule]()
    var rulesNA = [String]()
    var rulesPassed = [String]()
    var rulesFailed = [String]()
    
    mutating func addRule(rule: SCPRule) {
        rules[rule.id] = rule
        switch rule.finding {
        case true:
            rulesPassed.append(rule.id)
        case false:
            rulesFailed.append(rule.id)
        default:
            rulesNA.append(rule.id)
        }
    }
}
