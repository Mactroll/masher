//
//  MobileConfigurationProfile.swift
//  masher
//
//  Created by jcadmin on 6/15/20.
//  Copyright © 2020 Jamf. All rights reserved.
//

import Foundation

struct MobileConfigurationProfile: Encodable {
    let version: Int
    let displayName: String
    let identifier: String
    let type: String
    let uuid: String
    let scope: String
    let organization: String
    let description: String
    let payload: [[String:MobileConfigValue]]
    
    enum CodingKeys: String, CodingKey {
        case version = "PayloadVersion"
        case uuid = "PayloadUUID"
        case type = "PayloadType"
        case scope = "PayloadScope"
        case description = "PayloadDescription"
        case organization = "PayloadOrganization"
        case identifier = "PayloadIdentifier"
        case displayName = "PayloadDisplayName"
        case payload = "PayloadContent"
    }
    
    init(payload: [String:MobileConfigValue]) {
        self.version = 1
        self.uuid = UUID.init().uuidString
        self.type = "Configuration"
        self.scope = "System"
        self.organization = "NIST macOS Security Guidelines"
        self.description = "Sample Profile"
        self.payload = [flattenConfig(config: payload)]
        self.displayName = "Testing"
        self.identifier = "menu.nomad.masher." + self.uuid
    }
    
    init(rule: SCPRule, payload: [String:MobileConfigValue]) {
        self.version = 1
        self.uuid = UUID.init().uuidString
        self.type = "Configuration"
        self.scope = "System"
        self.organization = "NIST macOS Security Guidelines"
        self.description = rule.discussion
        self.payload = [flattenConfig(config: payload)]
        self.displayName = rule.title
        self.identifier = "menu.nomad.masher." + self.uuid
    }
}

func flattenConfig(config: [String:MobileConfigValue]) -> [String:MobileConfigValue] {
    
    var payload = [String:MobileConfigValue]()
    
    for object in config {
        if object.key == kManagedClientDomain {
            switch object.value {
            case .object(let value):
                payload = flattenConfig(config: value)
                return payload
            default:
                break
            }
        }
        
        switch object.value {
        case .object(let value):
            payload = value
        default:
            break
        }
        payload["PayloadType"] = MobileConfigValue.string(object.key)
        payload["PayloadUUID"] = MobileConfigValue.string(UUID.init().uuidString)
        payload["PayloadVersion"] = MobileConfigValue.int(1)
    }
    return payload
}
