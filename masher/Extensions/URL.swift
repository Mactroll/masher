//
//  URL.swift
//  masher
//
//  Created by jcadmin on 6/15/20.
//  Copyright © 2020 Orchard & Grove, Inc. All rights reserved.
//

import Foundation

extension URL {
    var isDirectory: Bool {
        return (try? resourceValues(forKeys: [.isDirectoryKey]))?.isDirectory == true
    }
}
