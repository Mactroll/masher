//
//  GitHelper.swift
//  masher
//
//  Created by jcadmin on 6/15/20.
//  Copyright © 2020 Orchard & Grove, Inc. All rights reserved.
//

import Foundation

let kDefaultLocalRepoPath = "/var/tmp/macos_security"

struct GitHelper {
    
    let kdefaultRepo = "https://github.com/usnistgov/macos_security.git"
    
    func getRepo(repo: String?=nil) {
        let task = Process()
        task.launchPath = "/usr/bin/git"
        task.arguments = ["clone", repo ?? kdefaultRepo, kDefaultLocalRepoPath]
        task.launch()
        task.waitUntilExit()
    }
}
