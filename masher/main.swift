//
//  main.swift
//  masher
//
//  Created by Joel Rennich on 6/11/20.
//  Copyright © 2020 Orchard & Grove, Inc. All rights reserved.
//

import Foundation
import Yams

var path: String

if CommandLine.arguments.count < 2 {
    path = kDefaultLocalRepoPath
    if !FileManager.default.fileExists(atPath: path) {
        GitHelper().getRepo()
    }
} else {
    path = CommandLine.arguments[1]
}

var ruleURLs = [URL]()
var yamlRules = SCPRules()
var rulesPassed = 0
var rulesFailed = 0
var rulesNA = 0

print("Parsing Rules from: \(path ?? "Unknown")")

let folders = try FileManager.default.contentsOfDirectory(at: URL.init(fileURLWithPath: path + "/rules"), includingPropertiesForKeys: [URLResourceKey.isDirectoryKey], options: .skipsHiddenFiles)

for folder in folders {
    let temprules = try FileManager.default.contentsOfDirectory(at: folder, includingPropertiesForKeys: [], options: .init())
    ruleURLs.append(contentsOf: temprules)
}

let decoder = YAMLDecoder.init()

for rule in ruleURLs {
    if rule.absoluteString.contains(".yaml") {
        if let yamlText = try? String.init(contentsOf: rule),
        var yamlRule = try? decoder.decode(SCPRule.self, from: yamlText) {
            //print("Found rule: \(yamlRule.id)")
            //print("\t Check Result: \(yamlRule.checkRule())")
            yamlRule.checkRule()
            yamlRules.addRule(rule: yamlRule)
        } else {
            print(rule.absoluteString)
        }
    }
}

print("Total Rules Checked: \(String(yamlRules.rules.count))")
print("Total Passed: \(String(yamlRules.rulesPassed.count))")
for rule in yamlRules.rulesPassed {
    //print("\t \(rule)")
}
print("Total NA: \(String(yamlRules.rulesNA.count))")
print("Total Failed: \(String(yamlRules.rulesFailed.count))")
for rule in yamlRules.rulesFailed {
    //print("\t \(rule)")
}
